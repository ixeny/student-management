﻿
DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(20) default NULL,
  `marks` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

/*Data for the table `students` */

insert  into `students`(`id`,`name`,`marks`) values (1,'Cao',12),(2,'Larissa',34),(5,'Lynda',56),(6,'Xavier',78);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;