/**
 * Created by Devesh on 24-01-2015.
 */
var mysql = require('mysql');
var connection = mysql.createConnection({
    host     : 'localhost',
    database : 'student_database',
    user     : 'dbuser',
    password : 'password'
});

module.exports = connection;
